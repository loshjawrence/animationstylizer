#include <maya/MPxCommand.h>
#include <maya/MFnPlugin.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/MGlobal.h>
#include <maya/MSimple.h>

MStatus initializePlugin(MObject obj)
{
	MStatus   status = MStatus::kSuccess;
	MFnPlugin plugin(obj, "MyPlugin", "1.0", "Any");

	// Register Command
	// status = plugin.registerCommand("LSystemCmd", LSystemCmd::creator, LSystemCmd::newSyntax);
	if (!status) {
		status.perror("registerCommand");
		return status;
	}

	// Set name
	plugin.setName("Animation Stylizer");

	// Load UI from MEL file
	MGlobal::executeCommand("source \"" + plugin.loadPath() + "/GUI.mel\"");
	status = plugin.registerUI("makeASMenu", "removeASMenu");

	return status;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus   status = MStatus::kSuccess;
	MFnPlugin plugin(obj);

	// Deregister Command
	//status = plugin.deregisterCommand("LSystemCmd");
	
	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}
	return status;
}
