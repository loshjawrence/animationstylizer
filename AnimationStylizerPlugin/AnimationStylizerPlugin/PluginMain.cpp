#include <maya/MPxCommand.h>
#include <maya/MFnPlugin.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/MGlobal.h>
#include <maya/MSimple.h>
#include <maya/MFnPlugin.h>

MString pluginMenuName;

MStatus initializePlugin(MObject obj)
{
	MStatus   status = MStatus::kSuccess;
	MFnPlugin plugin(obj, "MyPlugin", "1.0", "Any");

	// Register Command

	MString addLSystemMenu = "menu -parent MayaWindow -label \"LSystems\";";

	MStringArray temp;
	MString pathToMenu = MGlobal::executeCommandStringResult(addLSystemMenu);
	pathToMenu.split('|', temp);
	pluginMenuName = temp[temp.length() - 1];

	MString addLSystemMenuItem = "menuItem -parent " + pluginMenuName + " -label \"LSystem Command\" -command LSystemGUICmd;";
	MGlobal::executeCommand(addLSystemMenuItem);
	MString addLSystemMenuItem2 = "menuItem -parent " + pluginMenuName + " -label \"LSystemNode Command\" -command LSystemNodeCmd;";
	MGlobal::executeCommand(addLSystemMenuItem2);

	if (!status) {
		status.perror("registerCommand");
		return status;
	}

	return status;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus   status = MStatus::kSuccess;
	MFnPlugin plugin(obj);

	MGlobal::executeCommand("deleteUI -m " + pluginMenuName);

	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}

	return status;
}
