copy ffmpeg folder to $(ProjectDir) (where the main.cpp and .vcproj lives)

project properties -> toolbar -> platform = win32/All configurations
main window -> toolbar release or debug but set to x86 to match with win32

project properties-> linker -> advanced -> Image has safe exception handlers = No(SAFESEH:NO) 
project properties-> linker -> optimization -> use link time code generation(/LTCG)
project properties-> linker -> build events ->post build events-> copy ../ffmpeg/bin* "$(OutDir)"
project properties->c/c++->general->additional lib dependencies->
ffmpeg\include
ffmpeg\include\libavcodec
ffmpeg\include\libavdevice
ffmpeg\include\libavfilter
ffmpeg\include\libavformat
ffmpeg\include\libavutil
ffmpeg\include\libpostproc
ffmpeg\include\libswresample
ffmpeg\include\libswscale

project properties->linker->general->additional lib dependencies-> 
ffmpeg\lib

project properties->linker->input->additional dependencies-> 
avcodec.lib
avdevice.lib
avformat.lib
avutil.lib
swscale.lib

add to ffmpeg/include/libavutil/common.h
#ifndef UINT64_C
#define UINT64_C(c) (c ## ULL)
#endif





/*****************************************
  *************INFO***********************
  ****************************************/

======================how to compile in MSVC
https://trac.ffmpeg.org/wiki/CompilationGuide/MSVC
http://www.ayobamiadewole.com/Blog/How-to-build-x264-or-libx264.dll-in-Windows

The reason this comes up again and again is because you're using encoding_example.c as your reference. Please don't do that. The most fundamental mistake in this example is that it doesn't teach you the difference between codecs and containers. In fact, it ignored containers altogether.
What is a codec? A codec is a method of compressing a media type. H264, for example, will compress raw video. Imagine a 1080p video frame, which is typically in YUV format with 4:2:0 chroma subsampling. Raw, this is 1080*1920*3/2 bytes per frame, i.e. ~3MB/f. For 60fps, this is 180MB/sec, or 1.44 gigabit/sec (gbps). That's a lot of data, so we compress it. At that resolution, you can get pretty quality at a few megabit/sec (mbps) for modern codecs, like H264, HEVC or VP9. For audio, codecs like AAC or Opus are popular.
What is a container? A container takes video or audio (or subtitle) packets (compressed or uncompressed) and interleaves them for combined storage in a single output file. So rather than getting one file for video and one for audio, you get one file that interleaves packets for both. This allows effective seeking and indexing, it typically also allows adding metadata storage ("author", "title") and so on. Examples of popular containers are MOV, MP4 (which is really just mov), AVI, Ogg, Matroska or WebM (which is really just matroska).
(You can store video-only data in a file if you want. For H264, this is called "annexb" raw H264. This is actually what you were doing above. So why didn't it work? Well, you're ignoring "header" packets like the SPS and PPS. These are in avctx->extradata and need to be written before the first video packet. Using a container would take care of that for you, but you didn't, so it didn't work.)
How do you use a container in FFmpeg? See e.g. this post, particularly the sections calling functions like avformat_write_*() (basically anything that sounds like output). I'm happy to answer more specific questions, but I think the above post should clear out most confusion for you.

======================"this" post mentioned in previous answer
http://stackoverflow.com/questions/34823545/cutting-mpeg-ts-file-via-ffmpegwrapper/34984037#34984037

======================ffmpeg examples
https://www.ffmpeg.org/doxygen/0.6/output-example_8c-source.html
https://ffmpeg.org/doxygen/trunk/api-example_8c-source.html
https://ffmpeg.org/doxygen/trunk/encoding-example_8c-source.html



===============================other post with pseudo code for adding frame to video each time an image is created
http://stackoverflow.com/questions/34511312/how-to-encode-a-video-from-several-images-generated-in-a-c-program-without-wri


=================================get ffmpeg to work with other libs
http://www.ffmpeg.org/general.html#SEC24



You can learn a great deal from the source of the command-line utilities maintained by the FFmpeg project.
In ffplay.c, the main() will show you how to get the library initialized. stream_component_open() demonstrates matching codecs to streams in the media, and get_video_frame() shows how to decode a packet and get its PTS (presentation time stamp). You'll need that to time your splits correctly.
That should get you started on the decode side. On the encode side, look at ffmpeg.c. It's larger and more complicated than ffplay, but the process of encoding a frame nearly mirrors the process of decoding it, so once you have decoding working, it should make more sense.
